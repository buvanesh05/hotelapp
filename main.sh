#! /bin/bash

touch /var/hotelapp/helloworld

# git clone https://gitlab.com/buvanesh05/hotelapp.git /var/www/html/hotelapp

# curl -sS https://getcomposer.org/installer | php
# mv composer.phar /usr/local/bin/composer

# # Make sure that the PATH is set correctly
# export PATH="$PATH:/usr/local/bin"

# Install Laravel
composer global require laravel/installer

# Move the application to the Apache directory
mv /var/hotelapp/* /var/www/html/



sed -i "s/short_open_tag = .*/short_open_tag = On/" /etc/php7.4/apache2/php.ini
/usr/sbin/apache2ctl -D FOREGROUND